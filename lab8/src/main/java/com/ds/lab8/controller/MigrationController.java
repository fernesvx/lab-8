package com.ds.lab8.controller;

import com.ds.lab8.service.MigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MigrationController {
    @Autowired
    private MigrationService migrationService;

    @GetMapping("/migrate")
    public String migrateData(){
        migrationService.migrateData();
        return "DATA MIGRATED successful!";
    }

    @GetMapping("/migrate/eletronic")
    public String migrateEletronicData(){
        migrationService.migrateEletronicsData();
        return "DATA MIGRATED successful!";
    }
}
