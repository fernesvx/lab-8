package com.ds.lab8.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@Document(collection = "eletronics")
public class EletronicsMongo {
    @Id
    private String id;
    private String names;
    private BigDecimal price;
}
