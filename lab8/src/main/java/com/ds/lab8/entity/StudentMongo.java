package com.ds.lab8.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "aluno")
public class StudentMongo {
    @Id
    private String id;
    private String nome;
    private int idade;
}
