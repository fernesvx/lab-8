package com.ds.lab8.service;

import com.ds.lab8.entity.EletronicsMongo;
import com.ds.lab8.entity.EletronicsPostgre;
import com.ds.lab8.entity.StudentMongo;
import com.ds.lab8.entity.StudentPostgre;
import com.ds.lab8.repository.EletronicsMongoRepository;
import com.ds.lab8.repository.EletronicsPostgreRepository;
import com.ds.lab8.repository.StudentMongoRepository;
import com.ds.lab8.repository.StudentPostgreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MigrationService {
    @Autowired
    private StudentPostgreRepository studentPostgreRepository;
    @Autowired
    private StudentMongoRepository studentMongoRepository;
    @Autowired
    private EletronicsPostgreRepository eletronicsPostgreRepository;
    @Autowired
    private EletronicsMongoRepository eletronicsMongoRepository;

    public void migrateData(){
        List<StudentPostgre> students = studentPostgreRepository.findAll();
        for (StudentPostgre student : students){
            StudentMongo studentMongo = new StudentMongo();
            studentMongo.setNome(student.getNome());
            studentMongo.setIdade(student.getIdade());

            studentMongoRepository.save(studentMongo);
        }
    }

    public void migrateEletronicsData(){
        List<EletronicsPostgre> eletronics = eletronicsPostgreRepository.findAll();
        for(EletronicsPostgre eletronic : eletronics){
            EletronicsMongo eletronicsMongo = new EletronicsMongo();
            eletronicsMongo.setNames(eletronic.getNames());
            eletronicsMongo.setPrice(eletronic.getPrice());

            eletronicsMongoRepository.save(eletronicsMongo);
        }
    }
}
