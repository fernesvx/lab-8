package com.ds.lab8.repository;

import com.ds.lab8.entity.EletronicsMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EletronicsMongoRepository extends MongoRepository<EletronicsMongo, String> {
}
