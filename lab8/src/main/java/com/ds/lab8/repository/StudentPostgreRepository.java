package com.ds.lab8.repository;

import com.ds.lab8.entity.StudentPostgre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentPostgreRepository extends JpaRepository<StudentPostgre, Long> {
}
