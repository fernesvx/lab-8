package com.ds.lab8.repository;

import com.ds.lab8.entity.StudentMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentMongoRepository extends MongoRepository<StudentMongo, String> {
}
