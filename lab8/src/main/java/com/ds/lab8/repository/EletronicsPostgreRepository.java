package com.ds.lab8.repository;

import com.ds.lab8.entity.EletronicsPostgre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EletronicsPostgreRepository extends JpaRepository<EletronicsPostgre, Long> {
}
